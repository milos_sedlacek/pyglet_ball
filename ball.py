import pyglet, pyglet.gl, math, random

framerate = 60.0
batch = pyglet.graphics.Batch()
group = pyglet.graphics.OrderedGroup(0)
#pyglet.gl.glColor4f(1.0,1.0,1.0,1.0)

class Border(object):
	def __init__(self, width, height):
		self.x = 0
		self.y = 0
		self.mx = self.x + width
		self.my = self.y + height

class Line(object):
	def __init__(self, x, y):
		self.start_x = x
		self.start_y = y
		self.final_x = x
		self.final_y = y
		self.line_batch = pyglet.graphics.Batch()
		self.line_batch.add(2, pyglet.gl.GL_LINES,group,('v2i', (self.start_x, self.start_y, self.final_x, self.final_y)),('c3B', (0, 255, 0, 255, 0, 0)))
		#pyglet.graphics.draw(2, pyglet.gl.GL_LINES,('v2i', (self.start_x, self.start_y, self.final_x, self.final_y)),('c3B', (0, 0, 255, 0, 255, 0)))
	
	def update(self, x, y):
		self.final_x = x
		self.final_y = y
		del self.line_batch
		self.line_batch = pyglet.graphics.Batch()
		self.line_batch.add(2, pyglet.gl.GL_LINES,group,('v2i', (self.start_x, self.start_y, self.final_x, self.final_y)),('c3B', (0, 255, 0, 255, 0, 0)))
		#pyglet.graphics.draw(2, pyglet.gl.GL_LINES,('v2i', (self.start_x, self.start_y, self.final_x, self.final_y)),('c3B', (0, 0, 255, 0, 255, 0)))
	
	def stop(self):
		del self
		
class Ball(object):
	def __init__(self, xy, vxy, parent):
		self.radius = 10
		self.diameter = self.radius * 2
		self.x = xy[0] - self.radius
		self.y = xy[1] - self.radius
		self.vx = vxy[0]
		self.vy = vxy[1]
		self.ax = 0.0
		self.ay = -9.80665
		self.rgb = (random.randrange(0,255),random.randrange(0,255),random.randrange(0,255))
		
		#self.correction = 0.95
		self.correction = random.random()
		
		self.framerate = parent.framerate
		self.border = parent.border

		self.image = pyglet.image.load("./images/ball.png")
		self.sprite = pyglet.sprite.Sprite(self.image, self.x, self.y, batch=batch)
		self.sprite.color = self.rgb
	
	def move(self):
		#self.vx += self.ax / self.framerate
		self.vy += self.ay / self.framerate
		self.x += self.vx
		self.y += self.vy
		#print self.vy
		
		self.check_collision()
		
		self.sprite.x = self.x
		self.sprite.y = self.y
	
	def check_collision(self):
		if self.x < self.border.x:
			self.vx = (self.vx * -1) * self.correction
			self.x = self.border.x
		elif self.x + self.diameter > self.border.mx:
			self.vx = (self.vx * -1) * self.correction
			self.x = self.border.mx - self.diameter
		if self.y < self.border.y:
			self.vy = (self.vy * -1) * self.correction
			self.y = self.border.y
		elif self.y + self.diameter > self.border.my:
			self.vy = (self.vy * -1) * self.correction
			self.y = self.border.my - self.diameter