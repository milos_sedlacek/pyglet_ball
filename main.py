import pyglet, pyglet.gl, math
from ball import *

class BallWindow(pyglet.window.Window):
	def __init__(self):
		super(BallWindow, self).__init__(500,500)
		self.fps_display = pyglet.clock.ClockDisplay()
		
		self.framerate = 100.0
		self.border = Border(self.width, self.height)
		self.balls = []
		self.line = None
		self.start_x = 0
		self.start_y = 0
		self.final_x = 0
		self.final_y = 0
		self.distance = 0
		self.speed = 0
		self.speed_vec = (0,0)
		pyglet.clock.schedule_interval(self.update, 1/self.framerate)

	def update(self,dt):
		if len(self.balls) > 0:
			for ball in self.balls:
				ball.move()

	def on_draw(self):
		self.clear()
		batch.draw()
		if self.line:
			self.line.line_batch.draw()
		self.fps_display.draw()
		
	def on_mouse_press(self, x, y, button, modifiers):
		self.line = Line(x,y)
		self.start_x = x
		self.start_y = y
	
	def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
		#pyglet.gl.glClear (pyglet.gl.GL_COLOR_BUFFER_BIT)
		self.line.update(x,y)
		#print self.line.final_x, self.line.final_y

	def on_mouse_release(self, x, y, button, modifiers):
		self.final_x = x
		self.final_y = y
		self.distance = math.hypot(self.start_x - self.final_x, self.start_y - self.final_y)
		self.line.stop()
		self.line = None
		
		if self.distance > 100:
			self.speed = 25
		elif self.distance > 75:
			self.speed = 20
		elif self.distance > 50:
			self.speed = 15
		elif self.distance > 25:
			self.speed = 10
		elif self.distance > 0:
			self.speed = 5
		else:
			self.speed = 0
		
		self.vxy = [0,0]
		if self.distance > 0:
			self.vxy[0] = (self.start_x - self.final_x) * (self.speed / self.distance)
			self.vxy[1] = (self.start_y - self.final_y) * (self.speed / self.distance)
		
		self.balls.append(Ball((x,y),self.vxy, self))

if __name__ == "__main__":
	window = BallWindow()
	pyglet.app.run()